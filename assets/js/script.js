$(function() {

	$(document)
		.on("submit", '#loginform', function(e) {
			// Login Start
			e.preventDefault();
			var formdata = $(this).serialize();
			$.post(BASE_URL+"action/login", formdata, function(res){
				if( res.status !== 200 ) {
					modalAlert(res.text);
					var domSel = ( res.status == 10 ) ? '#username' : '#password';
					$(domSel).val('').focus();
				} else location.reload();
			}, 'json');
			// Login End
		})
		.on("click", ".logout", function(e) {
			// Logout Start
			e.preventDefault();
			$.post(BASE_URL+"action/logout", function(res){
				if(res.status==0) {
					modalAlert(res.text);
					location.reload();
				}
			}, 'json');
			// Logout End
		}).on("change", "#image", function(e){
			var file = this.files;
			var reader = new FileReader();

			reader.onload = function(e) {
	      		$('#preview').attr('src', e.target.result);
		    }
			reader.readAsDataURL(this.files[0]);
		}).on("submit", "#saveform", function(e){
			e.preventDefault();

		    var formData = new FormData(this);

		    $.ajax({
		        url: BASE_URL+"action/save",
		        type: 'POST',
		        data: formData,
		        cache: false,
		        contentType: false,
		        processData: false,
		        dataType: 'json',
		        success: function (res) {
		            if( res.status == 200 ){
		            	modalAlert(res.text);
		            	// clear the form
		            	if( $('#id').val() !== '' ) {
							$.each(['first_name', 'last_name', 'email', 'image'], function(x,y){
								$("#"+y).val('');
							});
							$("#preview").attr('src', BASE_URL+'/assets/img_admin/default.jpg');
		            	} else location.reload();
		            } else {
						modalAlert(res.text);
						$("#email").val('').focus();
					} 
		        },
		    });

		}).on("click", ".delete", function(e) {
			e.preventDefault();
			var uid = $(this).attr('uid');
			console.log(uid);
			if( confirm("Are you sure you want to delete this user?") ) {
				// pressed ok
				$.post(BASE_URL+"action/delete",{id:uid},function(res){
					if( res.status == 200 )
						window.location.replace(BASE_URL);
				},'json');
			}
		});

});

function modalAlert(text) {
	// change to modals if required
	alert(text);
}