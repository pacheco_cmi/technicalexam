<?php $columns = ['first_name', 'last_name', 'email']; ?>

<table class="table">
	<thead>
		<tr>
			<th></th>
			<?php foreach($columns as $each): ?>
				<th><?=ucwords(str_replace('_', ' ', $each));?></th>
			<?php endforeach; ?>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ( $data as $row ) :?>
			<tr>
				<?php $img = $row->image!==''?$row->image:'default.jpg';?>
				<td><img class="rounded-circle" src="<?=base_url();?>assets/img_admin/<?=$img?>" width="30" height="30" /></td>
				<?php foreach($columns as $each): ?>
					<td><?=$row->{$each}?></td>
				<?php endforeach; ?>
				<td>
					<a href="<?=base_url();?>admin/user/<?=$row->id?>">Edit</a>
					<span class="mx-2"><b>|</b></span>
					<a href="" uid="<?=$row->id?>" class="delete">Delete</a>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<a href="<?=base_url();?>admin/user">
	<button class="btn btn-primary fixed-bottom-right rounded-circle">
		<h1 class="px-2">+</h1>
	</button>
</a>

<style type="text/css">
	.fixed-bottom-right {
		display: block;
		position: absolute;
		right: 50px;
		bottom: 50px;
		z-index: 100;
	}
</style>

<script type="text/javascript">
	$(function(){
		$(".table").dataTable();
	});
</script>
