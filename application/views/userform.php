<form id="saveform" method="post" enctype="multipart/form-data" action="">
<?php $data = array_filter($data); ?>
	<div class="row">
		
		<div class="col-6">
			
			<div class="form-group">
				<label for="first_name">First Name</label>
				<input type="text" required class="form-control" id="first_name" name="first_name" placeholder="First Name" value="<?=(isset($data['first_name'])>0)?$data['first_name']:'';?>">
			</div>

			<div class="form-group">
				<label for="first_name">Last Name</label>
				<input type="text" required class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="<?=(isset($data['last_name'])>0)?$data['last_name']:'';?>">
			</div>

			<div class="form-group">
				<label for="email">Email address</label>
				<input type="email" required class="form-control" id="email" name="email" placeholder="Email" value="<?=(isset($data['email'])>0)?$data['email']:'';?>">
			</div>

		</div>

		<div class="col-6">
			<label for="image">Profile Picture</label>
			<input type="file" required class="form-control" id="image" name="image" accept="image/*" />
			<div class="mx-auto text-center m-2">
				<?php $img = ( isset($data['image']) > 0 && $data['image'] !== '' ? $data['image'] : 'default.jpg' )?>
				<img width="150" height="150" id="preview" class="rounded-circle" src="<?=base_url();?>assets/img_admin/<?=$img;?>" />
			</div>
		</div>

	</div>

	<div class="form-group text-right mt-2">
		<input type="hidden" name="id" id="id" value="<?=(isset($data['id'])>0)?$data['id']:'';?>" />
		<input type="submit" class="btn btn-primary mx-3 submit" value="Save" />
		<?php if (isset($data['id'])>0) : ?>
			<input type="button" uid="<?=(isset($data['id'])>0)?$data['id']:'';?>" class="btn btn-primary mx-3 delete" value="Delete" />
		<?php endif; ?>
	</div>

</form>