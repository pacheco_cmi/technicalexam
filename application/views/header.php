<?php if( isset($admin_user) ): ?>
	<?php $admin_user['user_details'] = (object) $admin_user['user_details']; ?>
	<header class="p-1 mb-2" style="background:#f0f5ff;">
		
		<div class="row">
			
			<div class="col-7"></div>

			<div class="col-5 float-right text-right">
				<?php $img = $admin_user['user_details']->image!==''?$admin_user['user_details']->image:'default.jpg';?>
				<img class="rounded-circle" src="<?=base_url();?>/assets/img_admin/<?=$img;?>" width="50" height="50" />
				Welcome, <b><?=ucwords($admin_user['user_details']->first_name);?></b>
				<button class="btn btn-primary mx-3 logout">Logout</button>
			</div>
		</div>

	</header>

<?php endif; ?>