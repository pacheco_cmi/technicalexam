<div class="card mx-auto" style="width: 25rem; transform: translate(0, 50%);">
	<div class="card-body">
		<h5 class="card-title text-center mt-2">Login Form</h5>
		<!-- Login Form -->
		<form id="loginform">
			<div class="row p-2 m-2">
					
				<div class="col-12 m-2 pl-0">
			      	<input type="text" class="form-control" name="username" id="username" placeholder="Username">
			    </div>
			    <div class="col-12 m-2 pl-0">
			      	<input type="password" class="form-control" name="password" id="password" placeholder="Password">
			    </div>		
			    <div class="col-12 mt-3 mb-2 mx-auto pl-0 text-center">
			    	<button type="submit" class="btn btn-primary">Sign in</button>
			    </div>
			</div>
		</form>
	</div>
</div>