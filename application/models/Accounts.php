<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends CI_Model {

	private $tablename = 'accounts';

	public function checkLogin($username, $password) {
		$query = $this->db->get_where(
			$this->tablename, ['username'=>$username]
		);
		$rows = $query->result();

		// check username existence
		if( count($rows) == 0 ) 
			return ['status'=>10, 'text'=>'Username does not exist!'];

		// verify if password is correct
		$passVerfify = password_verify($password, $rows[0]->password);
		if( !$passVerfify ) 
			return ['status'=>11, 'text'=>'Password is Incorrect!'];

		// successful login!
		return ['status'=>200, 'text'=>'Logged In!', 'id'=>$rows[0]->id];
	}

}

?>