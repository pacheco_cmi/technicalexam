<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Model {

	private $tablename = 'users';

	public function getUserFromID( $id, $idtype = 'id' ) {
		$query = $this->db->get_where(
			$this->tablename, [$idtype=>$id]
		);
		$rows = $query->result();

		// check username existence
		if( count($rows) == 0 ) 
			return ['status'=>10, 'text'=>'User does not exist!'];

		// successful login!
		return ['status'=>200, 'data'=>(array)$rows[0]];
	}

	public function getUserList() {
		$query = $this->db->get($this->tablename);
		return $query->result();
	}

	public function save($data) {
		$data = array_filter($data);

		if( isset($data['id']) ) {
			$this->db->where('id', $data['id']);
			$this->db->update($this->tablename, $data);
		} else {
			// check email if existing first
			$query = $this->db->get_where(
				$this->tablename, ['email'=>$data['email']]
			);
			$rows = $query->result();
			if( count($rows) > 0 )
			 	return ['status'=>14, 'text'=>'Email is already being used!'];

			$this->db->insert($this->tablename, $data);
		}

		return ['status'=>200, 'text'=>'Saving Successful!'];
	}

	public function delete($id) {
		$query = $this->db->get_where($this->tablename, ['id'=>$id]);
		$rows = $query->result();

		// check username existence
		if( count($rows) == 0 ) 
			return ['status'=>10, 'text'=>'User does not exist!'];

		$this->db->delete($this->tablename, array('id' => $id));
		return ['status'=>200, 'text'=>'Delete Successful!'];
	}
}

?>