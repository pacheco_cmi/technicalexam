<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once "Base.php";
class Action extends Base {
	
	public function __construct() {
		parent::__construct();
		$this->sessioncheck();
	}

	public function login() {
		$params = $this->input->post(['username','password']);

		$this->load->model('accounts');
		$res = $this->accounts->checkLogin( 
			$params['username'], $params['password'] 
		);

		// add to session if logged in
		if( $res['status'] == 200 ) {
			$this->load->model('users');
			$userdata = $this->users->getUserFromID($res['id'], 'account_id');
			$_SESSION['admin'] = [
				'username' => $params['username'],
				'logtime' => strtotime('now'),
				'user_details' => $userdata['data']
			];
		}

		echo json_encode($res);
	}

	public function logout() {
		if( $this->isLoggedIn == true && isset($_SESSION['admin']) ) {
			unset($_SESSION['admin']);
			$this->isLoggedIn = false; // remove if redundant. assume memcached
			session_destroy();		
		}

		echo json_encode([
			'status'=>0, 'text'=>'Logged-out! See you Later!'
		]);
	}

	private function sessioncheck() {
		if( count($this->input->post()) > 0) {
			if( !in_array(uri_string(), ['action/login','action/logout']) ) {
				if( !$this->isLoggedIn == true || !isset($_SESSION['admin']) )
					die( json_encode(
						['status'=>0, 'text'=>'You have been Logged-out. Please Log In again!'] 
					));
			}
		} else redirect('/admin', 'refresh'); #non ajax access go back to admin
	}


	public function save() {
		// process file saving
		$oldfname = explode('.',$_FILES['image']["name"]);
		$filename = implode('', $this->input->post(['first_name','last_name'])).'_'.uniqid();
		$compfname = strtolower($filename).'.'.end($oldfname);
		$source = $_FILES['image']["tmp_name"];
		$dest = __DIR__."/../../assets/img_admin/".$compfname;
		copy ( $source, $dest );

		// save to db include image
		$datasave = $this->input->post();
		$datasave['image'] = $compfname;
		$this->load->model('users');
		$response = $this->users->save($datasave);
		
		echo json_encode($response);
	}

	public function delete() {
		$id = $this->input->post('id');
		$this->load->model('users');
		echo json_encode($this->users->delete($id));
	}
}