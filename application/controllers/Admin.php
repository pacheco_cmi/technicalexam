<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once "Base.php";
class Admin extends Base {
	
	public function __construct() {
		parent::__construct();
		$this->globalArr['TITLE'] = "Admin";

		// auto redir to login
		if( !$this->isLoggedIn && uri_string() !== 'admin/login' )
			redirect('/admin/login', 'refresh');
	}

	public function index() {
		$this->load->model('users');
		$this->loadToTemplate(
			'home', ['data'=>$this->users->getUserList()]
		);
	}

	public function login() {
		if( $this->isLoggedIn )
			redirect('/admin/index', 'refresh');

		$this->loadToTemplate('login');
	}

	public function user($id=0) {
		// if id == 0, assume a new form and is blank. Use generic save. CI allows this
		$this->load->model('users');
		$this->loadToTemplate(
			'userform', ($id!==0)?$this->users->getUserFromID($id):['data'=>[]]
		);
	}

}