<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base extends CI_Controller {

	// Set global variable for usage throughout template
	protected $globalArr = [];
	protected $isLoggedIn=false;

	public function __construct() {
		parent::__construct();
		if( !isset($_SESSION) ) session_start();

		if( isset($_SESSION['admin']) ) {
			$this->globalArr['admin_user'] = $_SESSION['admin'];
			$this->isLoggedIn = true;
		} 
	}

	protected function loadToTemplate($body, $arrData=[]) {
		$loadedVals = [];

		// Load generic view template blocks
		foreach(['RESOURCES', 'HEADER', 'FOOTER'] as $each)
			$loadedVals[$each] = $this->load->view(
				strtolower($each), $this->globalArr, true
			);
		
		// Load to body template after defining values
		$loadedVals['BODY'] = $this->load->view(
			$body, $arrData, true
		);

		// load to main
		return $this->load->view('main', $loadedVals);
	}
}
